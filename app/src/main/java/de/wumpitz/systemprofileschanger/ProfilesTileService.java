package de.wumpitz.systemprofileschanger;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.service.quicksettings.Tile;
import android.service.quicksettings.TileService;
import android.util.Log;

import cyanogenmod.app.Profile;
import cyanogenmod.app.ProfileManager;

public class ProfilesTileService extends TileService {
    private final String LOG_TAG = ProfilesTileService.class.getSimpleName();

    public ProfilesTileService() {
    }

    @Override
    public void onStartListening() {
        Log.d(LOG_TAG, "Start listening");
        Tile profilesTile = getQsTile();
        Log.d(LOG_TAG, "Got tile: " + profilesTile.getLabel());
        profilesTile.setState(Tile.STATE_ACTIVE);
        ProfileManager pm = ProfileManager.getInstance(getApplicationContext());
        Profile activeProfile = pm.getActiveProfile();
        profilesTile.setLabel(activeProfile.getName());
        profilesTile.updateTile();
        super.onStartListening();
    }


    @Override
    public void onClick() {
        Intent sendIntent = new Intent(this, MainActivity.class);
        startActivityAndCollapse(sendIntent);
        super.onClick();
    }
}
