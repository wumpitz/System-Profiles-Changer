package de.wumpitz.systemprofileschanger;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import java.util.UUID;

import cyanogenmod.app.Profile;
import cyanogenmod.app.ProfileManager;

public class MainActivity extends Activity {

    static final int PICK_PROFILE_REQUEST = 1;  // The request code

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent sendIntent = new Intent();
        ProfileManager pm = ProfileManager.getInstance(getApplicationContext());
        Profile activeProfile = pm.getActiveProfile();
        UUID profileUuid = activeProfile.getUuid();
        sendIntent.setAction(ProfileManager.ACTION_PROFILE_PICKER);
        sendIntent.putExtra(ProfileManager.EXTRA_PROFILE_EXISTING_UUID, profileUuid.toString());
        sendIntent.putExtra(ProfileManager.EXTRA_PROFILE_SHOW_NONE, false);
        startActivityForResult(sendIntent, PICK_PROFILE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == PICK_PROFILE_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                ProfileManager pm = ProfileManager.getInstance(getApplicationContext());
                String selected = data.getStringExtra(ProfileManager.EXTRA_PROFILE_PICKED_UUID);
                UUID selectedProfile = UUID.fromString(selected);
                //pm.getProfile(selectedProfile);
                pm.setActiveProfile(selectedProfile);
            }
        }
        finish();
    }
}
